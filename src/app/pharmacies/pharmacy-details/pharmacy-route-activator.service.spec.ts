/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { PharmacyRouteActivatorService } from './pharmacy-route-activator.service';

describe('PharmacyRouteActivatorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PharmacyRouteActivatorService]
    });
  });

  it('should ...', inject([PharmacyRouteActivatorService], (service: PharmacyRouteActivatorService) => {
    expect(service).toBeTruthy();
  }));
});
