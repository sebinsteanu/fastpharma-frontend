import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router'
import 'rxjs/add/operator/map'
import { DrugstoreService } from '../../_services';

@Injectable()
export class DrugstoreListResolverService implements Resolve<any>{

  constructor(private drugstoreService: DrugstoreService) { }

  resolve() {
    //returns an observable to the Angular framework
    return this.drugstoreService.getDrugstores().map(drugstores => {return drugstores});  
  }

}
