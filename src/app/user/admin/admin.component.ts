import { Component, OnInit, Inject } from '@angular/core';
import { UserService } from '../../_services';
import { AppConfig } from '../../app.config';
import { IUser } from '../../_models/user.model';
import { ActivatedRoute } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  currentUser: IUser;
  users: IUser[] = [];

  constructor(private userService: UserService, 
    private appConf: AppConfig, 
    private route: ActivatedRoute,
    private toastr: ToastrService,
    public dialog: MatDialog) {
      this.currentUser = JSON.parse(localStorage.getItem(appConf.localUserString));
  }

  ngOnInit() {
        this.users = this.route.snapshot.data['users']
        // this.loadAllUsers();
  }

  deleteUser(id: number) {
      this.userService.delete(id).subscribe(
        data => {
            // this.router.navigate([this.returnUrl]);
            this.loadAllUsers()
            this.toastr.success('User deleted');
        },
        error => {
            this.toastr.error('There was a problem while deleting the user');
        });
  }

  private loadAllUsers() {
      this.userService.getAll().subscribe(users => { this.users = users; });
  }

  
  editUser(id): void {
    this.userService.getById(id).subscribe(user => {
        let userToEdit:IUser = user
    
        let dialogRef = this.dialog.open(AdminModalDialog, {
        width: '750px',
        data: { username: userToEdit.username,
                firstName: userToEdit.firstName,
                lastName: userToEdit.lastName
            }
        });
    
        dialogRef.afterClosed().subscribe(result => {
          if(result){
            result.id = id
            this.userService.updateUser(result).subscribe()
            this.loadAllUsers()
            this.toastr.success('User updated');
          }
        });
    })
  }

}


@Component({
    templateUrl: 'admin-modal-dialog.html',
  })
  export class AdminModalDialog{
  
    constructor(
      public dialogRef: MatDialogRef<AdminModalDialog>,
      @Inject(MAT_DIALOG_DATA) public data: any) { }
  
    onClose(): void {
      this.dialogRef.close();
    }
  
  }