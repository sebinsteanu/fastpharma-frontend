﻿import { Injectable, OnInit, ViewContainerRef, Output, EventEmitter } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { AppConfig } from '../app.config';
import { IUser } from '../_models/user.model';

import { Observable } from "rxjs/Rx";
import { NetworkHelperService } from './helpers/network.helper.service';

import { map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class UserService{

    appUser: IUser
    usersUrl: string

    @Output() orderCountChange: EventEmitter<number> = new EventEmitter();

    constructor(
        private http: Http, 
        private config: AppConfig,
        private jwtHelper: NetworkHelperService,
        private toastr: ToastrService
    ) { 
        this.appUser = JSON.parse(localStorage.getItem(this.config.localUserString))
        this.usersUrl = this.config.apiUrl + '/users'
        console.log(this.appUser)
    }

    sendNewPasswordRequest(email){
        return this.http.get(this.usersUrl + "/forgot?email=" + email, this.jwtHelper.getJwtToken()).map(r => {
            this.toastr.success("Recover email sent!")
            return r
        })
    }
    
    //farmacist
    getFarmacistDrugstore(){
        return this.http.get(this.usersUrl + '/farmacist/drugstore', this.jwtHelper.getJwtToken()).pipe(map((response: Response) => response.json()))        
    }

    getAllOrders(){
        return this.http.get(this.usersUrl + '/farmacist/orders', this.jwtHelper.getJwtToken()).pipe(map((response: Response) => response.json()))        
    }
    executeOrder(invoiceId){
        return this.http.delete(this.usersUrl + '/farmacist/invoice/' + invoiceId, this.jwtHelper.getJwtToken()).map(r => r)
    }

    //client
    getOrdersCount(){
        return this.http.get(this.usersUrl + '/invoice/orders-count', this.jwtHelper.getJwtToken()).pipe(map((response: Response) => response.json()))
    }

    getActiveOrder(){
        return this.http.get(this.usersUrl + '/invoice/orders', this.jwtHelper.getJwtToken()).pipe(map((response: Response) => response.json()))
    }

    getSentOrders(){
        return this.http.get(this.usersUrl + '/invoice/sent', this.jwtHelper.getJwtToken()).pipe(map((response: Response) => response.json()))
    }

    checkoutOrders(){
        return this.http.get(this.usersUrl + '/invoice/checkout', this.jwtHelper.getJwtToken()).pipe(map(r => {
            this.toastr.success("Order sent succesfully!");
            this.orderCountChange.emit(0)
        }))
    }
    
    cancelOrder(orderId){
        console.log(orderId)
        return this.http.delete(this.usersUrl + '/invoice/' + orderId, this.jwtHelper.getJwtToken()).pipe(map(r => {
            this.toastr.success("Order canceled succesfully!");
            this.orderCountChange.emit(0)
        }))
    }


//users
    getAll() {
        return this.http.get(this.usersUrl , this.jwtHelper.getJwtToken()).pipe(map((response: Response) => response.json()))
    }

    getById(id: number) {
        return this.http.get(this.usersUrl + '/' + id, this.jwtHelper.getJwtToken()).pipe(map((response: Response) => response.json()))
    }

    register(user) {
        return this.http.post(this.usersUrl, user, this.jwtHelper.getJwtToken()).map(r => {
            this.toastr.success("Now you can login!", "Registered succesfully!")
        })
    }

    updateUser(user: IUser) {
        return this.http.put(this.usersUrl + '/' + user.id, user, this.jwtHelper.getJwtToken());
    }

    delete(id: number) {
        return this.http.delete(this.usersUrl + '/' + id, this.jwtHelper.getJwtToken());
    }
    
    
    private handleError(error: Response){
        return Observable.throw(error.statusText);
      }

    // private helper methods


    
    isClient: boolean = this.isAuthenticated() ? (this.appUser.userRoleId == 0 ? true : false) : false
    isFarmacist : boolean = this.isAuthenticated() ? (this.appUser.userRoleId == 1 ? true : false) : false
    isAdmin : boolean = this.isAuthenticated() ? (this.appUser.userRoleId == 2 ? true : false) : false

    ///extra methods
    isAuthenticated():boolean{
        this.appUser = JSON.parse(localStorage.getItem(this.config.localUserString));
        return !!this.appUser;
    }

    updateInfo(newValues){
        this.appUser.firstName = newValues.firstName
        this.appUser.password = newValues.password
        this.appUser.email = newValues.email
        this.updateUser(this.appUser).subscribe( r => {
            this.toastr.success("Info updated succesfully!")
        })
        localStorage.setItem(this.config.localUserString, JSON.stringify(this.appUser));
    }


    ///todo: remove these
    checkAuthenticationStatus(){
        return this.http.get('/api/currentIdentity').pipe(map((response: any) => {
            if(response._body){
                return response.json();
            } else {
                return {};
            }
        })).do(currentUser => {
            if(!!currentUser.userName){
                this.appUser = currentUser
            }
        }).subscribe()
    }
    
    updateCurrentUser(firstName: string, lastName: string){
        this.appUser.firstName = firstName
        this.appUser.lastName = lastName

        let headers = new Headers({'Content-Type': 'application/json'})
        let options = new RequestOptions({headers: headers})
        // return this.httpService.put(`/api/users/${this.currentUser.id}`, JSON.stringify(this.currentUser), options)
    }
}