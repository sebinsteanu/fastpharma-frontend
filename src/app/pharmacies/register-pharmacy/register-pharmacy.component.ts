import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register-pharmacy',
  templateUrl: './register-pharmacy.component.html',
  styleUrls: ['./register-pharmacy.component.css']
})
export class RegisterPharmacyComponent implements OnInit {

  isDirty:boolean = false
  latitude: number;
  longitude: number;
  geoIsProvided:boolean = true

  constructor(private router:Router) { }

  ngOnInit() {
    this.latitude = 46.77;
    this.longitude = 23.59;
  }

  onChoseLocation(event){
    console.log(event)
    this.latitude = event.coords.lat
    this.longitude = event.coords.lng
  }

  savePharma(formValues){
    console.log(formValues)
  }

  cancelRegistration(){
    this.router.navigate(['/pharmacies'])
  }
}
