import { TestBed, inject } from '@angular/core/testing';

import { DrugstoreRouteActivatorService } from './drugstore-route-activator.service';

describe('DrugstoreRouteActivatorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DrugstoreRouteActivatorService]
    });
  });

  it('should be created', inject([DrugstoreRouteActivatorService], (service: DrugstoreRouteActivatorService) => {
    expect(service).toBeTruthy();
  }));
});
