import { TestBed, inject } from '@angular/core/testing';

import { AllOrdersService } from './all-orders.service';

describe('AllOrdersService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AllOrdersService]
    });
  });

  it('should be created', inject([AllOrdersService], (service: AllOrdersService) => {
    expect(service).toBeTruthy();
  }));
});
