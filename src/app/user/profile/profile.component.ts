import { Component, OnInit } from '@angular/core'
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService, AuthenticationService } from '../../_services';
import { IUser } from '../../_models';
import { ToastrService } from 'ngx-toastr';

@Component({
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit{

  profileForm: FormGroup
  private firstName: FormControl
  private email: FormControl
  private password: FormControl

  constructor(
    private userService: UserService, 
    private authService: AuthenticationService, 
    private toastr: ToastrService,
    private router: Router,
  ) {
  }

  ngOnInit(){
    if(this.userService.isAuthenticated()){
      let appUser:IUser = this.userService.appUser
      this.firstName = new FormControl(appUser.firstName, [Validators.required, Validators.pattern('[a-zA-Z -]+')])
      this.email = new FormControl(appUser.email, [Validators.required, Validators.pattern('[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}')])
      this.password = new FormControl(appUser.password, Validators.required)
      this.profileForm = new FormGroup({
        firstName: this.firstName,
        email: this.email,
        password: this.password
      })
    }
    else{
      this.toastr.error('User not authenticated!');
    }
  }

  validateFirstName(){
    return this.firstName.valid || this.firstName.untouched;
  }

  validateEmail(){
    return this.email.valid || this.email.untouched;
  }

  validatePassword(){
    return this.password.valid || this.password.untouched;
  }

  saveProfile(formValues){
    if(this.profileForm.valid){
      this.userService.updateInfo(formValues)
      this.router.navigate(['/user/home'])
    }
  }
  

  logOut(){
    this.authService.logout();
    this.userService.appUser = undefined
    this.router.navigate(['/user/login'])
  }

  
}