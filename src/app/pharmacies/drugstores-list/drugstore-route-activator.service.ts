import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { UserService } from '../../_services';

@Injectable()
export class DrugstoreRouteActivatorService implements CanActivate {

  canActivate() {
    return this.userService.isAuthenticated();
  }
  constructor(private userService: UserService) { 

  }



}
