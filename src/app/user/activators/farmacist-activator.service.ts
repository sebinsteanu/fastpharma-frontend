import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { UserService } from '../../_services';

@Injectable()
export class FarmacistRouteActivatorService implements CanActivate {

  constructor(private userService:UserService) { }
  
  canActivate() {
    return this.userService.isFarmacist;
  }
}
