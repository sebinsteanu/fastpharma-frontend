import { Injectable } from '@angular/core';
import { Resolve, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router'
import 'rxjs/add/operator/map'
import { DrugsService } from '../../_services';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class DrugsResolverService implements Resolve<any>{

  constructor(
    private drugsService: DrugsService,
    private toastr: ToastrService) { }

  resolve(route: ActivatedRouteSnapshot) {
    //returns an observable to the Angular framework
    let drugstoreId = route.params['id']
    if (!drugstoreId){
      this.toastr.error("There was an error on getting the drugstore info")
      return;
    }
    return this.drugsService.getByDrugstoreId(drugstoreId)
        .map(drugs => {
            return drugs
        });  
  }

}
