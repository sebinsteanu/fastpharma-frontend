import { Injectable, OnInit, EventEmitter, Output } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { AppConfig } from '../app.config';
import { IUser } from '../_models/user.model';

import { Observable } from "rxjs/Rx";
import { UserService } from './user.service'
import { NetworkHelperService } from './helpers';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class DrugsService{

    @Output() orderCountChange: EventEmitter<string> = new EventEmitter();

    drugsUrl: string

    constructor(
        private http: Http, 
        private config: AppConfig,
        private jwtHelper: NetworkHelperService,
        private toastr: ToastrService
    ) { 
        this.drugsUrl = config.apiUrl + "/api/medicaments/"
    }

    getAll() {
        return this.http.get(this.drugsUrl, this.jwtHelper.getJwtToken()).map((response: Response) => response.json()).catch(this.handleError);
    }

    getById(id: number) {
        return this.http.get(this.drugsUrl+ id, this.jwtHelper.getJwtToken()).map((response: Response) => response.json()).catch(this.handleError);
    }

    getByDrugstoreId(drugstoreId: number){
        return this.http
        .get(this.config.apiUrl + '/api/drugstores/' + drugstoreId + "/medicaments", this.jwtHelper.getJwtToken())
        .map((response: Response) => response.json())
        .catch(this.handleError);
    }

    create(drug) {
        return this.http.post(this.drugsUrl, drug, this.jwtHelper.getJwtToken());
    }

    makeOrder(medicamentId:number, quantity: number){
        return this.http.get(`${this.drugsUrl}${medicamentId}/order?quantity=${quantity}`, this.jwtHelper.getJwtToken()).map(
            data =>{
                this.toastr.success(`Added ${quantity} to cart!`)
                this.orderCountChange.emit("new")
            },
            error => console.log(error)
        )
    }

    update(drug) {
        return this.http.put(this.drugsUrl+ drug.id, drug, this.jwtHelper.getJwtToken());
    }

    delete(id: number) {
        return this.http.delete(this.drugsUrl+ id, this.jwtHelper.getJwtToken());
    }
    
    private handleError(error: Response){
        return Observable.throw(error.statusText);
      }


}