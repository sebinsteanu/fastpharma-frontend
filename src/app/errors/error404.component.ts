import { Component } from '@angular/core'

@Component({
  template: `
    <h1 class="errorMsg">404 Not Found</h1>
  `,
  styles: [`
    .errorMsg { 
        font-size: 150px;
        margin-top:100px; 
        text-align: center; 
    }`]
})
export class Error404Component{
  constructor() {

  }

}