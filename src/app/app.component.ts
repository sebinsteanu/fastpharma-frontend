import { Component } from '@angular/core';
import { UserService } from './_services';

@Component({
  selector: 'app-root',
  template: `
    <app-navbar *ngIf="this.userService.isAuthenticated()">Loading navbar...</app-navbar>
    <router-outlet></router-outlet>
  `
})
export class AppComponent {
  /**
   *
   */
  constructor(public userService: UserService) {  }
}
