import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyDrugstoreComponent } from './my-drugstore.component';

describe('MyDrugstoreComponent', () => {
  let component: MyDrugstoreComponent;
  let fixture: ComponentFixture<MyDrugstoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyDrugstoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyDrugstoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
