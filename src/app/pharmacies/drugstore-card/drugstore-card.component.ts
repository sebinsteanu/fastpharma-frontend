import { Component, OnInit, Input } from '@angular/core';
import { IDrugstore } from '../../_models';  

import {Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { UserService, VoterService, DrugstoreService } from '../../_services';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-drugstore-card',
  templateUrl: './drugstore-card.component.html',
  styleUrls: ['./drugstore-card.component.css']
})
export class DrugstoreCardComponent implements OnInit {

  @Input() drugstore: IDrugstore
  geolocation: string;
  private voters: string[];
  public isFav: boolean = false;

  constructor(
    public dialog: MatDialog, 
    public userService: UserService, 
    private voterService: VoterService,
    private drugstoreService: DrugstoreService,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.voters = this.drugstore.voters!=null ? JSON.parse(this.drugstore.voters) : [];
    this.voters.forEach(voterName => {
      if(voterName == this.userService.appUser.username){
        this.isFav = true;
      }
    });
  }

  getStatusStyle(){
    if(this.drugstore.status && this.drugstore.status === 'open')
      return {color: '#9bd129', 'font-weight': 'bold'};
    else return {color: '#aa0a32', 'font-weight': 'bold'};
  }

  toggleFavourite(favDrugstore: IDrugstore){
    let username = this.userService.appUser.username;
    if(!this.userHasVoted(favDrugstore)){
      this.voterService.addVoter(favDrugstore, username).subscribe()
      this.voters.push(username)
      this.isFav = true
    }else{
      this.voterService.deleteVoter(favDrugstore, username).subscribe()
      this.voters = this.voters.filter(voter => voter != username)
      this.isFav = false
    }
  }

  private userHasVoted(favDrugstore: IDrugstore): boolean{
    return (this.voters!= null) ? this.voters.some(voter => voter === this.userService.appUser.username):false;
  }

  openMapDialog(): void {
    let dialogRef = this.dialog.open(DrugstoreMapModalDialog, {
      width: '750px',
      data: { lat: this.drugstore.address.latitude, long: this.drugstore.address.longitude }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.geolocation = result;
      console.log(this.geolocation)
    });
  }

  openEditDialog(drugstore): void {
    console.log(drugstore)
    if(drugstore){
      let dialogRef = this.dialog.open(DrugstoreEditModalDialog, {
        width: '750px',
        data: { 
          name: drugstore.name,
          street: drugstore.address.street,
          city: drugstore.address.city,
          number: drugstore.address.number,
          lat: drugstore.address.latitude, 
          long: drugstore.address.longitude,
          openingAt: drugstore.openingAt,
          closingAt: drugstore.closingAt
        }
      });

      dialogRef.afterClosed().subscribe(result => {
        console.log(result)
        if(result){
          this.drugstoreService.updateDrugstore(result)
            this.toastr.success("Drugstore updated succesfully!");
        }else{
          this.toastr.warning("Drugstore was not updated!");
        }
      });
    }
  }

}

@Component({
  templateUrl: 'drugstore-map-modal-dialog.html',
})
export class DrugstoreMapModalDialog {

  constructor(
    public dialogRef: MatDialogRef<DrugstoreMapModalDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onClose(): void {
    this.dialogRef.close();
  }

}


@Component({
  templateUrl: 'drugstore-edit-modal-dialog.html',
})
export class DrugstoreEditModalDialog {

  constructor(
    public dialogRef: MatDialogRef<DrugstoreMapModalDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onClose(): void {
    this.dialogRef.close();
  }

}