import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router'
import { ProfileComponent } from './profile/profile.component';
// import { userRoutes } from './user.routes';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { Routes } from "@angular/router";
import { AdminComponent} from './admin/admin.component';
import { AdminResolverService } from './admin/admin-resolver.service';
import { ProfileResolverService } from './profile/profile-resolver.service';
import { MatTabsModule } from '@angular/material';
import { OrdersComponent, OrderResolverService, SentOrdersResolverService } from './orders';
import { ClientRouteActivatorService, AdminRouteActivatorService, FarmacistRouteActivatorService } from './activators';
import { AllOrdersComponent } from './all-orders/all-orders.component';
import { AllOrdersResolverService } from './all-orders/all-orders.service';
import { MyDrugstoreComponent } from './my-drugstore/my-drugstore.component';
import { DrugstoreResolverService } from '../pharmacies/shared/drugstore-resolver.service';

const userRoutes:Routes = [
  {path: 'profile', component: ProfileComponent, resolve: {ProfileResolverService}},
  {path: 'orders', component: OrdersComponent, canActivate: [ClientRouteActivatorService] , resolve: {invoice: OrderResolverService, sentOrders: SentOrdersResolverService}},
  {path: 'all-orders', component: AllOrdersComponent, canActivate: [FarmacistRouteActivatorService] , resolve: {orders: AllOrdersResolverService}},
  {path: 'my-drugstore/:id', component: MyDrugstoreComponent, canActivate: [FarmacistRouteActivatorService], resolve: {drugstore: DrugstoreResolverService}},
  {path: 'home', component: AdminComponent, canActivate: [AdminRouteActivatorService], resolve: {users: AdminResolverService}},
  {path: 'login', component: LoginComponent}
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(userRoutes),
    MatTabsModule
  ],
  declarations: [
    ProfileComponent,
    LoginComponent,
    AdminComponent,
    OrdersComponent,
    AllOrdersComponent,
    MyDrugstoreComponent
  ],
  providers:[
    AdminRouteActivatorService,
    AdminResolverService,
    ClientRouteActivatorService,
    DrugstoreResolverService,
    FarmacistRouteActivatorService,
    
    ProfileResolverService,
    OrderResolverService,
    SentOrdersResolverService,
    AllOrdersResolverService
  ]
})
export class UserModule { }
