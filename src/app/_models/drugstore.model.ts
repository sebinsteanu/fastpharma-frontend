export interface IDrugstore{
    id: number
    name: string
    rating: number
    logo: string
    address: {
        city: string
        street: string
        number: number
        latitude: number
        longitude: number
    },
    lastUpdate: Date

    phone?: string
    status?: string
    openingAt?: string
    closingAt?: string
    website?: string
    voters?: string
}