/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { DrugstoreService } from './drugstore.service';

describe('DrugstoreService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DrugstoreService]
    });
  });

  it('should ...', inject([DrugstoreService], (service: DrugstoreService) => {
    expect(service).toBeTruthy();
  }));
});
