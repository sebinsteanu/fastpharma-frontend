/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { DrugstoreListResolverService } from './drugstore-list-resolver.service';

describe('DrugstoreListResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DrugstoreListResolverService]
    });
  });

  it('should ...', inject([DrugstoreListResolverService], (service: DrugstoreListResolverService) => {
    expect(service).toBeTruthy();
  }));
});
