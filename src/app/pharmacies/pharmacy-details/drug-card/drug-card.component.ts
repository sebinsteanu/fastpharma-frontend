import { Component, OnInit, Input } from '@angular/core';
import { IDrug } from '../../../_models';
import { DrugsService } from '../../../_services';

@Component({
  selector: 'app-drug-card',
  templateUrl: './drug-card.component.html',
  styleUrls: ['./drug-card.component.css']
})
export class DrugCardComponent implements OnInit {
  
  @Input() drug: any

  constructor(private drugService: DrugsService) { }

  ngOnInit() {
  }

  addToCart(medicamentId, quantity){
    console.log(medicamentId)
    
    this.drugService.makeOrder(medicamentId, quantity).subscribe()
  }
}
