﻿import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
// import { Observable } from 'rxjs/Observable';

import { Observable } from "rxjs/Rx";
import 'rxjs/add/operator/map'

import { AppConfig } from '../app.config';
import { IUser } from '../_models/user.model';
import { UserService } from '.';

@Injectable()
export class AuthenticationService {

    currentUser: IUser

    constructor(private httpService: Http, private config: AppConfig) { 

        // let user =  localStorage.getItem("user");
        // this.currentUser = JSON.parse(user)
    }

    login(username: string, password: string) {
        let url = this.config.apiUrl + '/users/authenticate';
        var response = this.httpService.post(url, {username: username, password: password})
            .map((response: Response) => {
                    // login successful if there's a jwt token in the response
                    let user = response.json();
                    if (user && user.token) {
                        // store user details and jwt token in local storage to keep user logged in between page refreshes
                        localStorage.setItem(this.config.localUserString, JSON.stringify(user));
                    }
            }).catch(this.handleError);
        return response
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem(this.config.localUserString);
        localStorage.removeItem(this.config.ordersCount)
    }
    
    private handleError(error: Response){
        return Observable.throw(error.statusText);
      }



  ///todo: delete this method
  loginUser(username: string, password: string){
    let headers = new Headers({'Content-Type': 'application/json'})
    let options = new RequestOptions({headers: headers})
    let loginInfo = {username: username, password: password}

    localStorage.setItem("user", JSON.stringify(loginInfo));

        return this.httpService.post('/api/login', JSON.stringify(loginInfo), options).do( response => {
            if(response){
        //         this.currentUser = <IUser> response.json().user
                localStorage.setItem("user", JSON.stringify(this.currentUser));
            }
        }).catch(error => {
            return Observable.of(false);
        })
    }
}
