import { Injectable } from '@angular/core';
import { UserService } from '../../_services';
import { Resolve } from '@angular/router';
import 'rxjs/add/operator/map'

@Injectable()
export class AdminResolverService implements Resolve<any>{

  constructor(private userService: UserService) { }

  resolve() {
    //returns an observable
    return this.userService.getAll().map((response: Response) => {
      return response
    })
  }
}
