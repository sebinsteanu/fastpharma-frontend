import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { UserService } from '../../_services';

@Injectable()
export class ClientRouteActivatorService implements CanActivate {

  constructor(private userService:UserService) { }
  
  canActivate() {
    return this.userService.isClient;
  }
}
