
export interface IDrug {
    id: number
    drugstoreId: number
    imageUrl: string
    name: string
    quantity: string
    price: number
    diseaseTarget: string
    weight: string
    producerId: number
    usingMode: string
    prospect: string
}