import { Injectable, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { IDrugstore } from '../_models';
import { HttpClient } from '@angular/common/http';
import { AppConfig } from '../app.config';
import { NetworkHelperService } from './helpers';
import { Http, Response } from '@angular/http';

@Injectable()
export class DrugstoreService {

  private drugstoreApiUrl:string;

  constructor(
    private http:Http, 
    private config: AppConfig,
    private networkHelper: NetworkHelperService
  ) { 
    this.drugstoreApiUrl = this.config.apiUrl + '/api/drugstores'
  }


  getDrugstores() : Observable<IDrugstore[]>{
    return this.http
      .get(this.drugstoreApiUrl, this.networkHelper.getJwtToken())
      .map(
        response => {
          console.log(JSON.parse(response.text()))
          return <IDrugstore> JSON.parse(response.text())
        },
        error => console.log(error)
      )
      .catch(this.networkHelper.handleError)

    // let subject = new Subject<IDrugstore[]>()
    // setTimeout(() => {
    //   subject.next(DRUGSTORES); subject.complete();
    // }, 10);

    // return subject;
  }

  getDrugstore(id: number) : Observable<IDrugstore>{
    console.log(id)
    return this.http
    .get(this.drugstoreApiUrl + "/" + id, this.networkHelper.getJwtToken())
    .map(
      response => {  
        console.log(JSON.parse(response.text()))
        return <IDrugstore> JSON.parse(response.text())
      }
    );
  }

  updateDrugstore(drugstore: IDrugstore){
    return this.http.put(this.drugstoreApiUrl + "/" + drugstore.id, drugstore, this.networkHelper.getJwtToken())
    .map(
      resp =>{
        console.log(resp)
        return resp
      }
    )
  }
}

// const DRUGSTORES : IDrugstore[] = [{
//     id: 5,
//     name: "Ducfarm",
//     status: "open",
//     rating: 4,
//     phone: "0723490323",
//     openingAt: '09:00AM',
//     closingAt: '20:00PM',
//     logo: 'https://www.starbt.ro/img/parteneri/1415718182dUCFARM.png',
//     address: {
//       city: "Cluj-Napoca",
//       street: "Str. Albac",
//       number: 17,
//       latitude: 45.45,
//       longitude: 45.45
//     },
//     lastUpdate: new Date('10/05/2018'),
//     website: ""
//   },
//   {
//     id: 3,
//     name: "Catena",
//     status: "closed",
//     openingAt: '09:00AM',
//     closingAt: '17:30PM',
//     rating: 4,
//     phone: "0800 83 45 32",
//     logo: 'https://www.catena.ro/assets/uploads/files/images/Nico%20si%20Enache%20dec%202017.jpg',
//     address: {
//       city: "Cluj-Napoca",
//       street: "Str. Albac",
//       latitude: 46.76,
//       longitude: 23.61,
//       number: 17
//     },
//     lastUpdate: new Date('10/05/2018'),
//     website: ""
//   },
//   {
//     id: 2,
//     name: "Dona",
//     status: "open",
//     openingAt: '09:00AM',
//     closingAt: '17:00PM',
//     rating: 4,
//     phone: "0723490323",
//     logo: 'https://static.unica.ro/wp-content/uploads/2016/10/logo-farmacia-Dona2.jpg',
//     address: {
//       city: "Cluj-Napoca",
//       street: "Str. Albac",
//       latitude: 45.45,
//       longitude: 45.45,
//       number: 17
//     },
//     lastUpdate: new Date('10/05/2018'),
//     website: ""
//   },
//   {
//     id: 2,
//     name: "Richter",
//     status: "open",
//     openingAt: '09:00AM',
//     closingAt: '17:00PM',
//     rating: 4,
//     phone: "0723490323",
//     logo: 'http://static.metropola.info/pictures/fullsize/1000/default-image-gallery/6252___wd.jpg',
//     address: {
//       city: "Cluj-Napoca",
//       street: "Str. Albac",
//       latitude: 44.46,
//       longitude: 26.07,
//       number: 17
//     },
//     lastUpdate: new Date('10/05/2018'),
//     website: ""
//   }
// ]