import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../_services';
import { DrugstoreService } from '../../_services/drugstore.service';
import { IDrugstore } from '../../_models';

@Component({
  templateUrl: './drugstores-list.component.html',
  styleUrls: ['./drugstores-list.component.css']
})
export class DrugstoresListComponent implements OnInit {

  drugstores: IDrugstore[]

  constructor(private drugstoreService: DrugstoreService, 
    private route: ActivatedRoute,
  
  
    public userService: UserService,
    private drugstoresService: DrugstoreService
    // private spinnerService: LoaderSpinnerService
  ) {}

  ngOnInit() {
    this.drugstores = this.route.snapshot.data['drugstores']
  }

  handleCardClick(name){
  }
}
