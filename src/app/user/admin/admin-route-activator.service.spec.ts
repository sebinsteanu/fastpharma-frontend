import { TestBed, inject } from '@angular/core/testing';

import { AdminRouteActivatorService } from './admin-route-activator.service';

describe('AdminRouteActivatorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminRouteActivatorService]
    });
  });

  it('should be created', inject([AdminRouteActivatorService], (service: AdminRouteActivatorService) => {
    expect(service).toBeTruthy();
  }));
});
