import { Injectable } from '@angular/core';
import { UserService } from '../../_services';
import { Resolve, CanActivate } from '@angular/router';
import 'rxjs/add/operator/map'

@Injectable()
export class LoginRouteActivatorService implements CanActivate{

  constructor(private userService: UserService) { }

  canActivate() {
    //returns an observable
    return this.userService.isAuthenticated()
  }
}
