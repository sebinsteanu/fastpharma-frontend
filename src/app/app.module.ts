import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { AgmCoreModule } from '@agm/core';

import {FlexLayoutModule} from '@angular/flex-layout'

import { AngularFontAwesomeModule } from 'angular-font-awesome';

import {
  DrugstoresListComponent,
  DrugstoreCardComponent,
  PharmacyDrugsComponent,
  DrugstoreListResolverService,
  PharmacyRouteActivatorService,
  RegisterPharmacyComponent,
  DrugstoreMapModalDialog,
  DrugstoreEditModalDialog,
  UpvoteComponent,
  DrugsResolverService
} from './pharmacies/index'

import { AppComponent } from './app.component';
import { appRoutes } from './routes';
import { Error404Component } from './errors/error404.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatCardModule,
  MatGridListModule,
  MatToolbarModule,
  MatIconModule,
  MatMenuModule,
  MatDialogModule,
  MatInputModule,
  MatFormFieldModule,
  MatTooltipModule,
  MatDividerModule
} from '@angular/material';

import { DrugstoreRouteActivatorService } from './pharmacies/drugstores-list/drugstore-route-activator.service';
import { AuthenticationService, UserService, DrugstoreService, VoterService, NetworkHelperService, DrugsService } from './_services';
import { AppConfig } from './app.config';
import { AdminModalDialog } from './user/admin/admin.component';
import { DrugstoreResolverService } from './pharmacies/shared/drugstore-resolver.service';
import { DrugComponent } from './pharmacies/pharmacy-details/drug/drug.component';
import { DrugCardComponent } from './pharmacies/pharmacy-details/drug-card/drug-card.component';

import { ToastrModule, ToastContainerModule } from 'ngx-toastr';
import { NavbarComponent } from './nav/navbar/navbar.component';

@NgModule({
  declarations: [
    NavbarComponent,
    AppComponent,
    DrugstoresListComponent,
    DrugstoreCardComponent,
    PharmacyDrugsComponent,
    RegisterPharmacyComponent,
    Error404Component,
    DrugstoreMapModalDialog,
    DrugstoreEditModalDialog,
    AdminModalDialog,
    UpvoteComponent,
    DrugComponent,
    DrugCardComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatTooltipModule,
    MatButtonModule,
    MatCardModule,
    MatGridListModule,
    MatToolbarModule,
    FlexLayoutModule,
    MatIconModule,
    MatMenuModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    AngularFontAwesomeModule,
    MatDividerModule,

    RouterModule.forRoot(appRoutes),
    ToastContainerModule,
    ToastrModule.forRoot({ 
      positionClass: 'toast-top-center',
      newestOnTop: true,
      messageClass: "",
      titleClass: "",
      maxOpened: 5,
      autoDismiss: true
    }),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyA7Ts3BbcKG3_Ly5eLChEsCmDjzF-gsJ6s'
    })
  ],

  entryComponents:[
    DrugstoreMapModalDialog,
    DrugstoreEditModalDialog,
    AdminModalDialog
  ],

  providers: [
    DrugstoreService, 
    NetworkHelperService,
    PharmacyRouteActivatorService,
    DrugstoreRouteActivatorService,
    // {
    //   provide: 'canDeactivateNewPharma',
    //   useValue: true
    //   // useValue: checkState
    // },
    DrugstoreListResolverService,
    AppConfig,
    AuthenticationService,
    UserService,
    VoterService,
    DrugstoreResolverService,
    DrugsService,
    DrugsResolverService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

// function checkState(component: RegisterPharmacyComponent){
//   if(component.isDirty){
//     return window.confirm('You have unsaved changes! Do you still wanna leave?');
//   }
//   return true
// }