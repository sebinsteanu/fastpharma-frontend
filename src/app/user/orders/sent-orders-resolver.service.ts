import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { UserService } from '../../_services';

@Injectable()
export class SentOrdersResolverService implements Resolve<any> {

    constructor(private userService: UserService) { }

    resolve(){
        return this.userService.getSentOrders().map(resp => {
            return resp;
        })
    }
}