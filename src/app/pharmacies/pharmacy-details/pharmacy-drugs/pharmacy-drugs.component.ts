import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { DrugstoreService } from '../../../_services';
import { IDrugstore } from '../../../_models';

@Component({
  templateUrl: './pharmacy-drugs.component.html',
  styleUrls: ['./pharmacy-drugs.component.css']
})
export class PharmacyDrugsComponent implements OnInit {

  drugstore: IDrugstore
  drugs: any
  latitude: number
  longitude: number

  constructor(
    private drugsService:DrugstoreService, 
    private activatedRoute:ActivatedRoute
  ) { }

  ngOnInit() {

    
    this.drugstore = this.activatedRoute.snapshot.data['drugstore']
    this.drugs = this.activatedRoute.snapshot.data['drugs']

    this.latitude = this.drugstore.address.latitude
    this.longitude = this.drugstore.address.longitude
  }


  onChoseLocation(event){
    console.log(event)
    this.latitude = event.coords.lat
    this.longitude = event.coords.lng
  }

}
