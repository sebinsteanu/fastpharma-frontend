import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../_services';

@Component({
  selector: 'app-all-orders',
  templateUrl: './all-orders.component.html',
  styleUrls: ['./all-orders.component.css']
})
export class AllOrdersComponent implements OnInit {

  activeOrders:any

  constructor(private activatedRoute: ActivatedRoute, public userService: UserService) { }

  ngOnInit() {
    this.activeOrders = this.activatedRoute.snapshot.data['orders']
    console.log(this.activeOrders)
  }
  sendOrder(invoiceId) {
    this.userService.executeOrder(invoiceId).subscribe(r => {
      this.userService.getAllOrders().subscribe(ord => this.activeOrders = ord)
    })
  }

}
