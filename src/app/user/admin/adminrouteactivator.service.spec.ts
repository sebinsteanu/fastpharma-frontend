import { TestBed, inject } from '@angular/core/testing';

import { AdminrouteactivatorService } from './adminrouteactivator.service';

describe('AdminrouteactivatorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminrouteactivatorService]
    });
  });

  it('should be created', inject([AdminrouteactivatorService], (service: AdminrouteactivatorService) => {
    expect(service).toBeTruthy();
  }));
});
