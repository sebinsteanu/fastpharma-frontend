import { Routes } from '@angular/router'

import { 
    DrugstoresListComponent,
    PharmacyDrugsComponent,
    RegisterPharmacyComponent,
    PharmacyRouteActivatorService,
    DrugstoreListResolverService,
    DrugsResolverService
} from "./pharmacies/index";
import { Error404Component } from './errors/error404.component';
import { UserModule } from './user/user.module';
import { DrugstoreRouteActivatorService } from './pharmacies/drugstores-list/drugstore-route-activator.service';
import { DrugstoreResolverService } from './pharmacies/shared/drugstore-resolver.service';
import { DrugComponent } from './pharmacies/pharmacy-details/drug/drug.component';


export const appRoutes:Routes = [
    {path: 'pharmacies/new', component: RegisterPharmacyComponent, canDeactivate: ['canDeactivateNewPharma']},
    {path: 'pharmacies/:id', component: PharmacyDrugsComponent, canActivate: [PharmacyRouteActivatorService], resolve: {drugstore:DrugstoreResolverService, drugs:DrugsResolverService}},
    {path: 'pharmacies', component: DrugstoresListComponent, canActivate:[DrugstoreRouteActivatorService],  resolve : {drugstores:DrugstoreListResolverService} },
    {path: 'medicaments/:id', component: DrugComponent},
    {path: '', redirectTo: '/user/login', pathMatch: 'full'},
    {path: '404', component: Error404Component},
    // {path: 'user', loadChildren: () => UserModule}
    {path: 'user', loadChildren: 'app/user/user.module#UserModule' }
]