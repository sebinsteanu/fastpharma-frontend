import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../_services';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {

  activeOrders: any
  sentOrders: any
  public shoppingCartTotal: number
  constructor(private activatedRoute: ActivatedRoute, private userService: UserService) { 
  }

  ngOnInit() {    
    this.activeOrders = this.activatedRoute.snapshot.data['invoice']
    this.sentOrders = this.activatedRoute.snapshot.data['sentOrders']
  }

  checkoutOrders(values){
    console.log(this.activeOrders)
    console.log(values)

    this.userService.checkoutOrders().subscribe( r=> {
      this.activeOrders = null
      this.userService.getSentOrders().subscribe( r => { this.sentOrders = r } )
    })
  }

  cancelOrder(id){
    this.userService.cancelOrder(id).subscribe(r => {
      this.userService.getSentOrders().subscribe( r => { this.sentOrders = r } )
    })
    
  }

  emptyShoppingCart(id){
    this.activeOrders = null
    this.cancelOrder(id)
  }
}
