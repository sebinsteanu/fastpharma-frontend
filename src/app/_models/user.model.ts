export interface IUser{
    id: number
    firstName: string
    lastName: string
    username: string
    password: string
    email: string
    userRoleId: number
    orders: number
    drugstoreId: number
}