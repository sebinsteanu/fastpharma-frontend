import { Component, OnInit, Input } from '@angular/core';
import { UserService, DrugsService } from '../../_services';
import { Router, CanActivate } from '@angular/router';
import { IUser } from '../../_models';
import { AppConfig } from '../../app.config';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  
  ordersCount: number = 0
  user: IUser

  constructor(
    public userService: UserService, 
    public drugsService: DrugsService,
    private router: Router,
    private conf: AppConfig
  ) {
    this.user = userService.appUser;
    
    this.refreshOrderCount()
  }

  ngOnInit() {
    
      this.userService.orderCountChange.subscribe(ordersCount => {
        this.ordersCount = ordersCount
      });

      this.drugsService.orderCountChange.subscribe(operation => {
        this.refreshOrderCount()
      });

  }

  refreshOrderCount(){
    if(this.userService.isClient){
      this.userService.getOrdersCount().subscribe(r => { this.ordersCount = r});
    }
  }
}
