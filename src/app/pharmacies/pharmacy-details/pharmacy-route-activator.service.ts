import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, Router } from '@angular/router';
import { DrugstoreService } from '../../_services';

@Injectable()
export class PharmacyRouteActivatorService implements CanActivate{

  constructor(private pharmaService: DrugstoreService, private router: Router) { }


  canActivate(route: ActivatedRouteSnapshot):boolean {
    const existsPharma = !!this.pharmaService.getDrugstore(+route.params['id']).map(drugstore => {return drugstore})
    if(!existsPharma){
      this.router.navigate(['/404'])
    }
    return existsPharma;
  }
}
