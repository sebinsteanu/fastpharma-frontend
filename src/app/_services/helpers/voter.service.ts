import { Injectable } from "@angular/core";
import { RequestOptions, Headers, Http, Response } from "@angular/http";
import { Observable, Subject } from "rxjs/Rx";
import { AppConfig } from "../../app.config";
import { NetworkHelperService } from "../../_services/helpers/network.helper.service";
import { IDrugstore } from "../../_models";


@Injectable()
export class VoterService{

    constructor(
        private httpService: Http,
        private config: AppConfig,
        private jwtHelper: NetworkHelperService
    ) {}

    deleteVoter(drugstore: IDrugstore, voterName: string): Observable<any>{
        let url = this.config.apiUrl + `/api/drugstores/${drugstore.id}/voters?voterName=${voterName}`
        return this.httpService.delete(url, this.jwtHelper.getJwtToken()).map(r => {return r})
    }

    addVoter(drugstore: IDrugstore, voterName: string) {
        let url = this.config.apiUrl + `/api/drugstores/${drugstore.id}/voters?voterName=${voterName}`
        return this.httpService.post(url, JSON.stringify({}), this.jwtHelper.getJwtToken()).map(r => {return r})
    }
}