import { Injectable } from "@angular/core";
import { RequestOptions, Headers } from "@angular/http";
import { AppConfig } from "../../app.config";


import { Observable } from "rxjs/Rx";

@Injectable()
export class NetworkHelperService{

    /**
     *
     */
    constructor(private config: AppConfig) {
    }

    public getJwtToken() {
        // create authorization header with jwt token
        let currentUser = JSON.parse(localStorage.getItem(this.config.localUserString));
        if (currentUser && currentUser.token) {
            let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token });
            return new RequestOptions({ headers: headers });
        }
    }

    
    public handleError(error: Response){
        console.log(error)
        return Observable.throw(error.statusText);
      }

}