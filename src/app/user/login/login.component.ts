import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService, UserService } from '../../_services';
import { ToastrService } from 'ngx-toastr';
import { IUser } from '../../_models';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  constructor(private authService: AuthenticationService, 
    private userService: UserService, 
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router) { }

  loading = false;
  returnUrl: string;
  mouseoverLogin: boolean;
  mouseoverRegister: boolean;
  selectedIndex: number = 0;

  ngOnInit() {
      // reset login status
      this.authService.logout();

      // get return url from route parameters or default to '/'
      this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  login(formValues){
    console.log(formValues)
    this.loading = true;
    this.authService.login(formValues.username, formValues.password).subscribe(
      user => {
          // this.router.navigate([this.returnUrl]);
          if(this.userService.isAdmin){
            this.router.navigate(['/user/home'])
          }else if (this.userService.isClient){
            this.router.navigate(['/pharmacies'])
          }else if(this.userService.isFarmacist){
            this.router.navigate(['/pharmacies'])
          }
      },
      error => {
          this.toastr.error('Username or password is incorrect');
          this.loading = false;
      });

  }

  goToForgot(){
    this.selectedIndex = 2
  }
  sendPasswordRequest(forgotForm){
    let email = forgotForm.email;
    this.userService.sendNewPasswordRequest(email).subscribe(r =>{
      console.log("email sent!")
    })
  }

  register(registerFormValues){
    console.log(registerFormValues)

    var user = {
      firstName : registerFormValues.firstName,
      password : registerFormValues.password,
      username : registerFormValues.username,
      email : registerFormValues.email
    }

    this.userService.register(user).subscribe( r => {
      console.log("registered!")
    })
  }



  // cancel(){
  //   this.router.navigate(['/pharmacies'])
  // }
}
